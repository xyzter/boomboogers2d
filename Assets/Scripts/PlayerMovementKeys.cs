using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMovementKeys : MonoBehaviour
{
    #region Singleton
    public static PlayerMovementKeys instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    Rigidbody2D rb;
   
    Animator anim;
    public float moveSpeed;
    
    public float x, y;
    private bool isWalking;

    private Vector3 moveDir;

    public float skill_time;

    bool using_skill;

    private void Start() {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void Update() {
        x = Input.GetAxisRaw("Horizontal");
        y = Input.GetAxisRaw("Vertical");

        #region SkillTime
        if (skill_time >= 0) skill_time -= Time.deltaTime;

        using_skill = (skill_time >= 0);

        if (using_skill) return;
        #endregion

        if (x != 0 || y != 0)
        {
            anim.SetFloat("x", x);
            anim.SetFloat("y", y);
            if(!isWalking)
            {
                isWalking = true;
                anim.SetBool("isMoving", isWalking);
            }
        }else 
        {
            if(isWalking)
            {
                isWalking = false;
                anim.SetBool("isMoving", isWalking);
                StopMoving();
            }
        }

        moveDir = new Vector3(x,y).normalized;
    }

    private void FixedUpdate() {

        if (using_skill) return;

        rb.velocity = moveDir * moveSpeed * Time.deltaTime;
        Vector3 moveVector = new Vector3(x, y).normalized;
        GetComponent<IMoveVelocity>().SetVelocity(moveVector);
        //rb.velocity = new Vector3(x * moveSpeed, 0.0f, y * moveSpeed);
    }

    void StopMoving()
    {
        rb.velocity = Vector3.zero;
    }

    public void StandStill(float time)
    {
        skill_time = time;
    }
}
