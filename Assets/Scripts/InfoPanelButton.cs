using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoPanelButton : MonoBehaviour
{
    [SerializeField]private GameObject info_panel;

    private bool is_active;

    public void TogglePanel()
    {
        if (is_active)
        {
            Debug.Log("oi");
            info_panel.SetActive(false);
            is_active = false;
            return;
        }
        if (!is_active)
        {
            Debug.Log("oi");
            info_panel.SetActive(true);
            is_active = true;
        }
    }
}
