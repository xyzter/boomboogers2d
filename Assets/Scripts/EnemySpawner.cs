using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private GameObject boss_enemy;
    private bool can_spawn = true;
    public int enemies_alive;
    private int enemies_to_spawn;
    [SerializeField] private GameObject medium_enemy;
    [SerializeField] private GameObject normal_enemy;

    [SerializeField] private Transform[] spawns;
    private float time_between_wave = 5f;
    private float type_of_enemy;

    private int wave = 1;

    [SerializeField] private bool survival;

    [SerializeField] private GameObject spawnParticle;


    private void Update()
    {
        #region cheats

        if (Input.GetKeyDown(KeyCode.L))
        {
            SceneManager.LoadScene(1);
        }

        #endregion


        if (time_between_wave > 0)
        {
            time_between_wave -= Time.deltaTime;
        }

        if (enemies_alive > 0)
        {
            can_spawn = false;
        }
        else if (enemies_alive <= 0 && time_between_wave <= 0)
        {
            can_spawn = true;
        }

        if (time_between_wave <= 0 && can_spawn)
        {
            can_spawn = false;
            SpawnEnemy();
            WaveCounter.instance.UpdateWave(wave);
        }
    }

    void SpawnEnemy()
    {
        enemies_to_spawn = wave;
        type_of_enemy = Random.Range(1, 101);

        if (wave % 10 == 0)
        {
            for (int i = (wave / 10); i > 0; i--)
            {
                StartCoroutine(nameof(Particles), 3);
            }
        }
        else
        {
            for (int i = enemies_to_spawn; i > 0; i--)
            {
                if (type_of_enemy <= 50)
                {
                    StartCoroutine(nameof(Particles), 1);
                }

                if (type_of_enemy > 50 && type_of_enemy < 90)
                {
                    StartCoroutine(nameof(Particles), 2);
                }
            }
        }

        wave++;

        time_between_wave = 5f;
    }

    IEnumerator Particles(int index)
    {
        
        int _temp = (int)Random.Range(0, spawns.Length - 1);
        Instantiate(spawnParticle, spawns[_temp].position, Quaternion.identity);

        yield return new WaitForSeconds(0.3f);
        if (index == 1)
        {
            SpawnNormal(_temp);
        }
        if (index == 2)
        {
            SpawnNMedium(_temp);
        }
        if (index == 3)
        {
            SpawnBoss(_temp);
        }
        yield return null;
    }

    void SpawnNormal(int _temp)
    {
        GameObject temporary = Instantiate(normal_enemy, spawns[_temp].position, Quaternion.identity) as GameObject;
        temporary.GetComponent<EnemyAI>().is_in_survival = true;
        enemies_alive++;
    }

    void SpawnNMedium(int _temp)
    {
        GameObject temporary = Instantiate(medium_enemy, spawns[_temp].position, Quaternion.identity) as GameObject;
        temporary.GetComponent<EnemyAI>().is_in_survival = true;
        enemies_alive++;
    }

    void SpawnBoss(int _temp)
    {
        GameObject temporary = Instantiate(boss_enemy, spawns[_temp].position, Quaternion.identity) as GameObject;
        temporary.GetComponent<EnemyAI>().is_in_survival = true;
        enemies_alive++;
    }

    #region singleton

    public static EnemySpawner instance;

    void Awake()
    {
        instance = this;
    }

    #endregion
}