using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseOverDescription : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler
{
    [SerializeField] private GameObject description;

    private void Start()
    {
        description.SetActive(false);
    }

    private void OnMouseOver()
    {
        description.SetActive(true);
    }

    private void OnMouseExit()
    {
        description.SetActive(false);
    }


    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        description.SetActive(true);
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
        description.SetActive(false);
    }
}
