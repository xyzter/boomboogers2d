using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSkills : MonoBehaviour
{
    #region Singleton
    public static PlayerSkills instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region FX

    [SerializeField] private GameObject EarthStompFX;
    [SerializeField] private GameObject MotherEarthCallFX;
    [SerializeField] private GameObject AudebaranFX;
    private GameObject _earth, _motherearth, _audebaran;

    #endregion


    [SerializeField] private Collider2D stomp_collider;

    //COOLDOWNS-------------------------------------
    public float earth_stomp_cooldown = 10f;
    public float mother_earth_cooldown = 90f;
    public float audebaran_blessing_cooldown = 60f;
    //----------------------------------------------
    //CAN USE---------------------------------------
    public bool can_earth = true;
    public bool can_mother = true;
    public bool can_audebaran = true;
    [SerializeField] private LayerMask enemymask;
    [SerializeField] private float stomp_radius;
    [SerializeField] private Image stomp;
    [SerializeField] private Image motherearth;
    [SerializeField] private Image audebaran;

    //public modifiers-------------
    [SerializeField] private float heal_amount;
    [SerializeField] private float stomp_damage;
    [SerializeField] private float stomp_stun;
    [SerializeField] private float shield_amount;

    // Update is called once per frame
    void Update()
    {
        
        if (earth_stomp_cooldown > 0) earth_stomp_cooldown -= Time.deltaTime;
        
        if (mother_earth_cooldown > 0) mother_earth_cooldown -= Time.deltaTime;

        if (audebaran_blessing_cooldown > 0) audebaran_blessing_cooldown -= Time.deltaTime;

        can_earth = (earth_stomp_cooldown <= 0);
        can_mother = (mother_earth_cooldown <= 0);
        can_audebaran = (audebaran_blessing_cooldown <= 0);

        //stomp.fillAmount = 1.0f / earth_stomp_cooldown * Time.deltaTime;
        motherearth.fillAmount += 1.0f / 90 * Time.deltaTime;
        audebaran.fillAmount += 1.0f / 60 * Time.deltaTime;
        stomp.fillAmount += 1.0f / 10 * Time.deltaTime;


        if (Input.GetKeyDown(KeyCode.B) && can_earth) EarthStomp();

        if (Input.GetKeyDown(KeyCode.N) && can_mother) MotherEarth();

        if (Input.GetKeyDown(KeyCode.M) && can_audebaran) AudebaranBlessing();


    }

    public void EarthStomp()
    {
        StompRange();
        //stomp_collider.enabled = true;
        _earth = Instantiate(EarthStompFX, transform.position, Quaternion.identity, transform) as GameObject;
        Invoke(nameof(ResetStopmFX), 0.7f);
        stomp.fillAmount = 0;
        earth_stomp_cooldown = 10f;

    }
    
    public void MotherEarth()
    {
        Healthbar.instance.Heal(heal_amount);
        GetComponent<PlayerMovementKeys>().StandStill(1.5f);
        _motherearth = Instantiate(MotherEarthCallFX, transform) as GameObject;
        Invoke(nameof(ResetMotherCallFX), 2f);
        motherearth.fillAmount = 0;
        mother_earth_cooldown = 90f;
    }

    public void AudebaranBlessing()
    {
        Healthbar.instance.armor += shield_amount;
        _audebaran = Instantiate(AudebaranFX, transform) as GameObject;
        Invoke(nameof(ResetArmor), 3f);
        audebaran.fillAmount = 0;
        audebaran_blessing_cooldown = 60f;
    }

    void StompRange()
    {
        Collider2D[] enemies = Physics2D.OverlapCircleAll(transform.position, stomp_radius, enemymask);
        foreach (Collider2D item in enemies)
        {
            item.gameObject.GetComponent<EnemyAI>().TakeDamage(stomp_damage);
            item.gameObject.GetComponent<EnemyAI>().Stagger(stomp_stun);
        }
    }

    void ResetArmor()
    {
        Healthbar.instance.armor = 0f;
        ResetAudebaranFX();
    }

    void ResetStopmFX()
    {
        Destroy(_earth);
    }

    void ResetMotherCallFX()
    {
        Destroy(_motherearth);
    }

    void ResetAudebaranFX()
    {
        Destroy(_audebaran);
    }
}
