using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowItems : MonoBehaviour
{
    public Color color;
    public Color new_color;
    SpriteRenderer this_sprite;
    // Start is called before the first frame update
    void Start()
    {
        this_sprite = GetComponent<SpriteRenderer>();
        color = this_sprite.color;
    }

    private void OnMouseOver() {
        this_sprite.color = new_color;
    }

    private void OnMouseExit() {
        this_sprite.color = color;
    }
}
