using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{

    public int reward;
    public int mana_reward;

    public bool isCollectable;
    public Item item;
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerInteraction>().coins += reward;
            other.gameObject.GetComponent<PlayerStats>().player_mana += mana_reward;
            if (this.gameObject.CompareTag("Coin"))
            {
                PlayerInteraction.instance.UpdateCollect();
            }

            
            if(other.gameObject.GetComponent<PlayerStats>().player_mana > other.gameObject.GetComponent<PlayerStats>().player_maxmana) 
                other.gameObject.GetComponent<PlayerStats>().player_mana = other.gameObject.GetComponent<PlayerStats>().player_maxmana;

            if(isCollectable)
            {
                bool canPickup = Inventory.instance.Add(item);
                if(canPickup) Destroy(this.gameObject);
                return;
            }

            Destroy(this.gameObject);

        }
    }
    
}
