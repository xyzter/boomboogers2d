using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextPlaceholder : MonoBehaviour
{
    [SerializeField] private Text _ref;


    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Text>().text = _ref.text;
    }
}
