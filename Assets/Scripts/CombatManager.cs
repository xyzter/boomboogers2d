using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatManager : MonoBehaviour
{
    public static CombatManager instance;
    public bool canReceiveInput = true;
    public bool inputReceived;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    public void Attack()
    {
            if(canReceiveInput)
            {
                inputReceived = true;
                canReceiveInput = false;
            }
            else
            {
                return;
            }
    }

    public void InputManager()
    {
       if(!canReceiveInput)
       {
           canReceiveInput = true;
       } 
       else{
           canReceiveInput = false;
       }
    }
}
