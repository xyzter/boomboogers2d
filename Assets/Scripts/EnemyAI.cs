using UnityEngine;
using UnityEngine.UIElements;

public class EnemyAI : MonoBehaviour
{
    private Animator anim;
    [Range(0, 100)] public float armor;

    //public Collider2D atkCollider;
    public float atkCooldown;
    public float attackRadius;
    public float checkRadius;
    [SerializeField] private GameObject coin_prefab;
    public float currentCooldown;
    public Vector3 dir;
    public int dmg;

    [SerializeField] [Header("Drop Chance")] [Range(0, 100)]
    private float drop_chance;

    //health
    public float health = 50;

    public GameObject healthBar;
    public bool isInAttackRange;

    private bool isInChaseRange;

    public float max_health;
    private Vector2 movement;

    [SerializeField] [Header("Item Raro")] private GameObject rare_item;
    private Rigidbody2D rb;
    public float speed;
    private Transform target;

    private bool test;
    public bool is_in_survival;

    public LayerMask what_is_player;

    [SerializeField] private bool is_boos;
    [SerializeField] private bool is_medium;

    [SerializeField] private GameObject particle;
    [SerializeField] private GameObject dmg_particle;

    public bool stunned;
    public float staggertime;

    [SerializeField] private LayerMask enemy;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        currentCooldown = atkCooldown;
        currentCooldown = 0f;
        //animator get component here
        anim = GetComponent<Animator>();
        target = GameObject.FindWithTag("Player").transform;
    }

    private void Update()
    {
        isInChaseRange = Physics2D.OverlapCircle(transform.position, checkRadius, what_is_player);
        isInAttackRange = Physics2D.OverlapCircle(transform.position, attackRadius, what_is_player);

        dir = target.position - transform.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        dir.Normalize();
        movement = dir;


        

        if (currentCooldown > 0) currentCooldown -= Time.deltaTime;

        if (staggertime > 0) staggertime -= Time.deltaTime;

        stunned = (staggertime > 0);
    }

    private void FixedUpdate()
    {
        if (stunned)
        {
            rb.velocity = Vector2.zero;
            return;
        }

        if (target.position.x > transform.position.x) anim.SetFloat("x", 1);

        if (target.position.x < transform.position.x) anim.SetFloat("x", -1);

        if (target.position.y > transform.position.y) anim.SetFloat("y", 1);

        if (target.position.y < transform.position.y) anim.SetFloat("y", -1);

        
        
        if (!isInAttackRange && is_in_survival)
        {
            MoveCharacter(movement);
        }
        

        if (isInChaseRange && !isInAttackRange && !is_in_survival)
        {
            MoveCharacter(movement);
        }

        if (!isInAttackRange && !isInChaseRange) rb.velocity = Vector2.zero;

        if (isInAttackRange)
        {
            rb.velocity = Vector2.zero;
            if (currentCooldown > 0) return;
            Attack();
            currentCooldown = atkCooldown;
            anim.SetTrigger("Atck One");
        }
    }

    void Attack()
    {
        Collider2D[] enemies = Physics2D.OverlapCircleAll(transform.position, 1, enemy);
        foreach (Collider2D item in enemies)
        {
            Healthbar.instance.SetDamageValue(dmg);
            Instantiate(dmg_particle, item.gameObject.transform.position + new Vector3(0, 1, 0), Quaternion.identity);
        }
    }

    private void MoveCharacter(Vector2 dir)
    {
        rb.MovePosition((Vector2) transform.position + dir * speed * Time.deltaTime);
    }

    public void TakeDamage(float dmg)
    {
        health -= dmg;
        PlayerInteraction.instance.UpdateDamage((int) dmg);

        if (health <= 0)
        {
            float _chance = Random.Range(1, 101);
            //Debug.Log(_chance);
            var _temp = Random.Range(1, 6);
            for (var i = _temp; i > 0; i--)
            {
                var coin = Instantiate(coin_prefab, transform.position, Quaternion.identity).GetComponent<Coin>();
                coin.reward = Random.Range(10, 21);
                coin.mana_reward = Random.Range(10, 26);
                coin.gameObject.transform.position += new Vector3(Random.Range(-1.5f, 1.5f), Random.Range(-1.5f, 1.5f),
                coin.transform.position.z);
            }

            if (_chance <= drop_chance && rare_item != null)
                Instantiate(rare_item, transform.position, Quaternion.identity);

            PlayerInteraction.instance.UpdateKill();
            if (is_in_survival)
            {
                SurvivalPoints.instance.UpdatePoints();
                EnemySpawner.instance.enemies_alive--;
            }


            if (is_boos)
            {
                Healthbar.instance.Heal(100);
            }
            if (is_medium)
            {
                Healthbar.instance.Heal(10);
            }
            Instantiate(particle, transform.position, Quaternion.identity);

            if (is_boos)
            {
                XPBar.instance.GetXp(30);
            }
            else
            {
                XPBar.instance.GetXp(1);
            }
            Destroy(gameObject);
            return;
        }

        healthBar.transform.localScale = new Vector3(health / 100, healthBar.transform.localScale.y,
            healthBar.transform.localScale.z);
        
        /*SetDamageValue(dmg);*/
    }

    
    public void SetDamageValue(float value)
    {
        health -= value;
        if (health - value < 0) value = health;
        health = (int) (healthBar.GetComponent<Slider>().value - value);
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && isInAttackRange && currentCooldown <= 0)
        {
            FindObjectOfType<Healthbar>().SetDamageValue(dmg);
            Instantiate(dmg_particle, target.transform.position, Quaternion.identity);
        }
    }

    public void Stagger(float time)
    {
        staggertime = time;
    }
}