using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour
{
    public GameObject loading_screen;
    public Slider slider;
    public Text progressText;

    public bool loginScren;

    public void InitiateLoadingAdventure()
    {

        loading_screen.SetActive(true);
        Invoke(nameof(LoadingAdventureStart), 2f);
    }

    void LoadingAdventureStart()
    {
        StartCoroutine(LoadAsync(3));
    }

    public void InitiateLoadingSurvival()
    {

        loading_screen.SetActive(true);
        Invoke(nameof(LoadingSurvival), 2f);
    }

    void LoadingSurvival()
    {
        StartCoroutine(LoadAsync(2));
    }

    IEnumerator LoadAsync(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            progressText.text = progress * 100f + "%";
            yield return null;
        }
    }
}
