using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment", menuName = "Inventory/Equipment")]
public class Equipment : Item
{
    public EquipmentSlot EquipmentSlot;
    public int armorModifier;
    public int damageModifier;
    public bool is_weapon;
    public bool is_melee;

    [Header("What Prefab")]
    public GameObject prefab;

    public override void Use()
    {
        base.Use();

        EquipmentManager.instance.Equip(this);
        //Equip and remove from inv
        RemoveFromInv();
    }

}

public enum EquipmentSlot { Head, Chest, Legs, WeaponMelee, WeaponRange, Shield, Mining}
