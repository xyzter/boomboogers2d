using UnityEngine;
using UnityEngine.EventSystems;

public class Attack : MonoBehaviour
{
    public static Attack instance;
    [SerializeField] private GameObject arrow_prefab;
    private bool can_fire = true;

    private float cooldown = 1;
    [SerializeField] public int cooldown_time;

    [HideInInspector] public bool is_melee;

    public Transform melee_attack;

    [SerializeField] public Transform weapon;

    [SerializeField] public int weapon_mana_cost;
    [SerializeField] public int weapon_mana_power;

    [Range(0, 30)] [SerializeField] public int weapon_power;
    [SerializeField] public int weapon_speed;

    //PlayerStats ps;

    private void Start()
    {
        //ps = GetComponent<PlayerStats>();
        instance = this;
        melee_attack = gameObject.transform.Find("MeleeCollider");
    }

    private void Update()
    {
        //if (EventSystem.current.IsPointerOverGameObject()) return;
        if (Input.GetMouseButtonDown(0) && can_fire && !is_melee) FireWeapon();

        if (Input.GetMouseButtonDown(1) && can_fire && !is_melee) FireWeaponMana();


        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.J) || Input.GetKeyDown(KeyCode.Z))
        {
            if (CombatManager.instance.canReceiveInput && is_melee) CombatManager.instance.Attack();
        }

        if (cooldown > 0)
        {
            cooldown -= 1f * Time.deltaTime;
            can_fire = false;
        }

        if (cooldown <= 0) can_fire = true;
    }

    private void FireWeapon()
    {
        float ArrowSpeed = weapon_speed;

        var angle = Utility.AngleTowardsMouse(weapon.position);
        var rot = Quaternion.Euler(new Vector3(0f, 0f, angle - 90f));

        var bullet = Instantiate(arrow_prefab, weapon.position, rot).GetComponent<Bullet>();
        bullet.ArrowVelocity = weapon_speed;
        bullet.dmg = weapon_power;

        can_fire = false;
        cooldown = cooldown_time;
    }

    private void FireWeaponMana()
    {
        float ArrowSpeed = weapon_speed;

        var angle = Utility.AngleTowardsMouse(weapon.position);
        var rot = Quaternion.Euler(new Vector3(0f, 0f, angle - 90f));

        var bullet = Instantiate(arrow_prefab, weapon.position, rot).GetComponent<Bullet>();
        bullet.ArrowVelocity = weapon_speed;
        bullet.dmg = weapon_mana_power;

        can_fire = false;
        cooldown = cooldown_time * 2;
    }
}