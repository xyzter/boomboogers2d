using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour
{

    #region Singleton
    public static EquipmentManager instance;

    private void Awake() {
        instance = this;
    }
    
    #endregion;

    Equipment[] currentEquip;
    public delegate void OnEquipmentChanged(Equipment newEquipment, Equipment oldEquipment);
    public OnEquipmentChanged onEquipmentChanged;
    Inventory inventory;

    public GameObject defaultMelee, defaultRange;

    private void Start() {
        int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        currentEquip = new Equipment[numSlots];
        inventory = Inventory.instance;
    }

    public void Equip(Equipment newItem)
    {
        int slotIndex = (int)newItem.EquipmentSlot;

        Equipment oldItem = null;

        if(currentEquip[slotIndex] != null)
        {
            oldItem = currentEquip[slotIndex];
            inventory.Add(oldItem);
            if(EquipmentSlot.WeaponMelee == newItem.EquipmentSlot)
            {
                PlayerInteraction _pi = FindObjectOfType<PlayerInteraction>();
                _pi.weapons[0] = newItem.prefab.gameObject;
                _pi.ChangeDamage();
            }
            if(newItem.EquipmentSlot == EquipmentSlot.WeaponRange)
            {
                PlayerInteraction _pi = FindObjectOfType<PlayerInteraction>();
                _pi.weapons[1] = newItem.prefab.gameObject;
                _pi.ChangeDamage();
            }
        }

        if(onEquipmentChanged != null)
        {
            onEquipmentChanged.Invoke(newItem, oldItem);
        }


            if(EquipmentSlot.WeaponMelee == newItem.EquipmentSlot)
            {
                PlayerInteraction _pi = FindObjectOfType<PlayerInteraction>();
                _pi.weapons[0] = newItem.prefab.gameObject;
                _pi.ChangeDamage();
            }
            if(newItem.EquipmentSlot == EquipmentSlot.WeaponRange)
            {
                PlayerInteraction _pi = FindObjectOfType<PlayerInteraction>();
                _pi.weapons[1] = newItem.prefab.gameObject;
                _pi.ChangeDamage();
            }
        currentEquip[slotIndex] = newItem;
    }

    public void Unequip (int slotIndex)
    {
        if(currentEquip[slotIndex] != null)
        {
            Equipment oldItem = currentEquip[slotIndex];
            inventory.Add(oldItem);

            if(EquipmentSlot.WeaponMelee == oldItem.EquipmentSlot)
            {
                PlayerInteraction _pi = FindObjectOfType<PlayerInteraction>();
                _pi.weapons[0] = defaultMelee;
                _pi.ChangeDamage();
            }
            if(oldItem.EquipmentSlot == EquipmentSlot.WeaponRange)
            {
                PlayerInteraction _pi = FindObjectOfType<PlayerInteraction>();
                _pi.weapons[1] = defaultRange;
                _pi.ChangeDamage();
            }

            currentEquip[slotIndex] = null;
           
            if(onEquipmentChanged != null)
            {
            onEquipmentChanged.Invoke(null, oldItem);
            }

        }
    }

    public void UnequipAll()
    {
        for (int i = 0; i < currentEquip.Length; i++)
        {
            Unequip(i);
        }
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.U)) UnequipAll();
        
    }
}
