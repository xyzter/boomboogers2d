using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectivesPanel : MonoBehaviour
{
    bool up;
    public GameObject panel;


    public void SwitchOnOff()
    {
        if(up)
        {
            panel.SetActive(false);
            up = false;
            return;
        }
        if(!up)
        {
            panel.SetActive(true);
            up = true;
        }
    }
}
