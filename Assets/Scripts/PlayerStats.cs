using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public int player_health { get; private set;}
    public int player_maxhealth = 100;
    public int player_mana;
    public int player_maxmana;

    public string player_class;
    
    public Stat damage;
    public Stat armor;

    private void Awake() {
        player_health = player_maxhealth;
        player_class = "Pedra";
    }

    public void TakeDamage(int dmg)
    {
        dmg -= armor.GetValue();

        dmg = Mathf.Clamp(dmg, 0, int.MaxValue);

        player_health -= dmg;
        Debug.Log(transform.name + " takes " + dmg + " damage.");

        if(player_health <= 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        Debug.Log(transform.name + " died.");
    }
}
