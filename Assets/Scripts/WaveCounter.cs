using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveCounter : MonoBehaviour
{
    #region singleton

    public static WaveCounter instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    private void Start()
    {
        UpdateWave(1);
    }

    public void UpdateWave(int _temp)
    {
        GetComponent<Text>().text = "WAVE: " + _temp.ToString();
    }
}
