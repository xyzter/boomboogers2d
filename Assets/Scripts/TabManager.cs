using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabManager : MonoBehaviour
{
    public InputField email_input;
    public InputField password_input;

    public int input_selected;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab) && Input.GetKey(KeyCode.LeftShift))
        {
            input_selected--;
            if (input_selected < 0) input_selected = 1;
            SelectInputField();
        }
        else if (Input.GetKeyDown(KeyCode.Tab))
        {
            input_selected++;
            if (input_selected > 1) input_selected = 0;
            SelectInputField();
        }

        void SelectInputField()
        {
            switch (input_selected)
            {
                case 0: email_input.Select();
                    break;
                case 1: password_input.Select();
                    break;
            }
        }
    }
/*
    public void EmailSelected() => input_selected = 0;
    public void PasswordSelected() => input_selected = 1;*/

}
