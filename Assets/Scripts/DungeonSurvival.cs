using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class DungeonSurvival : MonoBehaviour
{
    [SerializeField] private GameObject dungeonConfirmation;
    
    public void EnterTheDungeon()
    {
        SceneManager.LoadScene(2);
        dungeonConfirmation.SetActive(false);
    }

    public void EnterTheAdventure()
    {
        SceneManager.LoadScene(3);
        dungeonConfirmation.SetActive(false);
    }

    public void ExitTheGungeon()
    {
        dungeonConfirmation.SetActive(false);
    }
    /*public void OnMouseDown()
    {
        dungeonConfirmation.SetActive(true);
    }*/

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            dungeonConfirmation.SetActive(true);
        }
    }
}
