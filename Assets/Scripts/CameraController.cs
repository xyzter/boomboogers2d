using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public Vector3 camera_offset;
    public float follow_speed = 10f;
    public float x_min = -100f;
    // camera zoom
    float maxFov = 9f;
    float minFov = 3f;
    [SerializeField] float sens = 10f;
    [SerializeField] Camera cam;
    //-----------
    Vector3 vel = Vector3.zero;

    private void FixedUpdate() {
        Vector3 targetPos = target.position + camera_offset;
        Vector3 smoothed_pos = Vector3.SmoothDamp(transform.position, targetPos, ref vel, follow_speed * Time.fixedDeltaTime);

        transform.position = smoothed_pos;
        
        
    }

    private void Update() {
        float fov = cam.orthographicSize;
        fov += Input.GetAxis("Mouse ScrollWheel") * sens;
        fov = Mathf.Clamp(fov, minFov, maxFov);
        cam.orthographicSize = fov;
    }
/*
    var minFov: float = 15f;
 var maxFov: float = 90f;
 var sensitivity: float = 10f;
 
 function Update () {
   var fov: float = Camera.main.fieldOfView;
   fov += Input.GetAxis("Mouse ScrollWheel") * sensitivity;
   fov = Mathf.Clamp(fov, minFov, maxFov);
   Camera.main.fieldOfView = fov;
 }
 */
}
