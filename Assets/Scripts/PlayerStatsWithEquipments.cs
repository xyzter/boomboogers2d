using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatsWithEquipments : CharacterStats
{
    private void Start() {
        EquipmentManager.instance.onEquipmentChanged += OnEquipmentChanged;
    }

    void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
    {
        if(newItem != null)
        {
        armor.AddMod(newItem.armorModifier);
        damage.AddMod(newItem.damageModifier);
        }

        if(oldItem != null)
        {
            armor.RemoveMod(oldItem.armorModifier);
            damage.RemoveMod(oldItem.damageModifier);

        }
    }
}
