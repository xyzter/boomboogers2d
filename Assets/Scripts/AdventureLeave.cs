using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AdventureLeave : MonoBehaviour
{
    private bool player_arrived;
    private float time_to_leave = 5f;
    private float real_cooldown;

    [SerializeField] private Text countdown;

    private void Start()
    {
        real_cooldown = time_to_leave;
    }

    private void Update()
    {
        if (player_arrived)
        {
            real_cooldown -= Time.deltaTime;
            countdown.gameObject.SetActive(true);
            countdown.text = real_cooldown.ToString();
        }


        if(real_cooldown <= 0) SceneManager.LoadScene("new");
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            player_arrived = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player_arrived = false;
            real_cooldown = time_to_leave;
            countdown.gameObject.SetActive(false);
        }
    }

}
