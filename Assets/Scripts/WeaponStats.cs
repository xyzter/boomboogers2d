using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponStats : MonoBehaviour
{
    [SerializeField] public int dmg;
    [SerializeField] public int speed;
    [SerializeField] public int mana_cost;
    [SerializeField] public int mana_dmg;
    [SerializeField] public int cooldown_time;
    [SerializeField] public bool is_melee;
    [SerializeField] public GameObject particle;

    private void OnTriggerEnter2D(Collider2D other) {
        if(is_melee)
        {
            if (other.GetComponent<EnemyAI>() != null)
            {
                other.GetComponent<EnemyAI>().TakeDamage(dmg);
                Instantiate(particle, other.gameObject.transform.position, Quaternion.identity);
            }
            return;
        }
    }


    public void UpdateStatsMelee(int damage)
    {
        dmg = damage;
    }

}
