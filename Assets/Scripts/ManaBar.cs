using UnityEngine;
using UnityEngine.UI;

public class ManaBar : MonoBehaviour
{
    public static ManaBar instance;
    private int currentValue;

    private float damage;
    private bool loseHealth;

    private int maxValueInt;
    public Text mnText;

    public Slider slider;

    private void Awake()
    {
        instance = this;
        SetMaxValue(100);
        maxValueInt = (int) slider.maxValue;
        GetMana(slider.maxValue);
        currentValue = (int) slider.value;
        mnText.text = currentValue + "/" + maxValueInt;
    }

    private void Update()
    {
        if (slider.value > slider.maxValue) slider.value = slider.maxValue;

        /*
        if(Input.GetKeyDown(KeyCode.M))
        {
            LoseMana(Random.Range(15, 30));
        }
        if(Input.GetKeyDown(KeyCode.B))
        {
            GetMana(Random.Range(15, 30));
        }
        */
    }

    public void SetMaxValue(float value)
    {
        slider.maxValue = value;
        maxValueInt = (int) slider.maxValue;
        currentValue = (int) slider.value;
        mnText.text = currentValue + "/" + maxValueInt;
    }

    public void LoseMana(float value)
    {
        if (currentValue - value < 0) value = currentValue;
        slider.value -= value;
        if (slider.value < 0) slider.value = 0;
        currentValue = (int) slider.value;
        mnText.text = currentValue + "/" + maxValueInt;
    }

    public void GetMana(float value)
    {
        slider.value += value;
        if (slider.value > 0) slider.value = slider.maxValue;
        currentValue = (int) slider.value;
        mnText.text = currentValue + "/" + maxValueInt;
    }
}