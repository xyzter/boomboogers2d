using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public GameObject loading_screen;
    public Slider slider;
    public Text progressText;

    public bool loginScren;
    public InputField wallet;
    
    public void InitiateLoading(int sceneIndex)
    {
        if (loginScren)
        {
            
        }
        
        loading_screen.SetActive(true);
        Invoke(nameof(LoadingStart), 4f);
    }

    void LoadingStart()
    {
        StartCoroutine(LoadAsync(1));
    }

    IEnumerator LoadAsync(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            progressText.text = progress * 100f + "%";
            yield return null;
        }
    }
    
    public void GoTOMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
