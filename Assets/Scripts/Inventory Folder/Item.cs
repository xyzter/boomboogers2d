
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject, IMoveable
{
    new public string name = "New Item";

    [SerializeField]
    private int stackSize;

    public Sprite icon;

    private SlotScript slot;


    public bool isDefaultItem = false;

    public Sprite MyIcon { 
        get
        {
            return icon;
        }
    }

    public int MyStackSize { 
        get
        {
            return stackSize;
        }
     }

    public SlotScript MySlot {
        get{
            return slot;
        }

        set{
            slot = value;
        }
     }


    public virtual void Use()
    {

    }

    public void RemoveFromInv()
    {
        Inventory.instance.Remove(this);   
    }

    public void Remove()
    {
        if(MySlot != null)
        {
            MySlot.RemoveItem(this);
        }
    }
}
