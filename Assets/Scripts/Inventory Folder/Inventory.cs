using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{   
    public delegate void OnItemChanged();

    #region  Singleton



    private void Awake() {
        
        if(instance == null) instance = this;
    }

    #endregion
    public int space = 20;

    public OnItemChanged onItemChangedCallback;
    public static Inventory instance;
    public List<Item>items = new List<Item>();

    public bool Add(Item item)
    {
        if(!item.isDefaultItem) 
        {
            if(items.Count >= space)
            {
                Debug.Log("Cant add");
                return false;
            }
            items.Add(item);
            Debug.Log("added");

            if(onItemChangedCallback != null)
            {
                onItemChangedCallback.Invoke();
            }
        }

        return true;
    }

    public void Remove (Item item)
    {
        items.Remove(item);
        if(onItemChangedCallback != null)
            {
                onItemChangedCallback.Invoke();
            }
    }
}
