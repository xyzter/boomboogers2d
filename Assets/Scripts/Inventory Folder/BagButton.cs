using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BagButton : MonoBehaviour, IPointerClickHandler
{
    private Bag bag;

    [SerializeField] private Sprite full, empty;

    public Bag MyBag
    {
        get => bag;

        set
        {
            if (value != null)
                GetComponent<Image>().sprite = full;
            else
                GetComponent<Image>().sprite = empty;
            bag = value;
        }
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (InventoryScript.instance.FromSlot != null && HandScript.instance.MyMoveable != null &&
                HandScript.instance.MyMoveable is Bag)
                if (MyBag != null)
                    InventoryScript.instance.SwapBags(MyBag, HandScript.instance.MyMoveable as Bag);
            if (Input.GetKey(KeyCode.LeftShift)) HandScript.instance.TakeMoveable(MyBag);
        }

        else if (bag != null)
        {
            bag.myBagScript.OpenClose();
        }
    }

    public void RemoveBag()
    {
        InventoryScript.instance.RemoveBag(MyBag);
        MyBag.MyBagButton = null;

        foreach (var item in MyBag.myBagScript.GetItems()) InventoryScript.instance.AddItem(item);
        MyBag = null;
    }
}