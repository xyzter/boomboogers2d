using System.Collections.Generic;
using UnityEngine;

public class InventoryScript : MonoBehaviour
{
    [SerializeField] private BagButton[] bagButtons;

    private readonly List<Bag> bags = new List<Bag>();


    private SlotScript fromSlot;

    [SerializeField] private Item[] items;

    public bool CanAddBag => bags.Count < 5;

    public int MyEmptySlotCount
    {
        get
        {
            var count = 0;

            foreach (var bag in bags) count += bag.myBagScript.MyEmptySlotCount;

            return count;
        }
    }

    public int MyTotalSlotCount
    {
        get
        {
            var count = 0;

            foreach (var bag in bags) count += bag.myBagScript.Slots.Count;

            return count;
        }
    }

    public SlotScript FromSlot
    {
        get => fromSlot;

        set
        {
            fromSlot = value;
            if (value != null) fromSlot.myIcon.color = Color.grey;
        }
    }

    public int MyFullSlotCount => MyTotalSlotCount - MyEmptySlotCount;

    // Start is called before the first frame update
    private void Start()
    {
        var bag = (Bag) Instantiate(items[0]);
        bag.Initialize(16);
        ((IUseable) bag).Use();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            var bag = (Bag) Instantiate(items[0]);
            bag.Initialize(8);
            ((IUseable) bag).Use();
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            var bag = (Bag) Instantiate(items[0]);
            bag.Initialize(16);
            AddItem(bag);
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            var potion = (HealthPotion) Instantiate(items[1]);
            AddItem(potion);
        }
    }

    public void AddBag(Bag bag)
    {
        foreach (var bagButton in bagButtons)
            if (bagButton.MyBag == null)
            {
                bagButton.MyBag = bag;
                bags.Add(bag);
                bag.MyBagButton = bagButton;
                break;
            }
    }

    public void AddBag(Bag bag, BagButton bagButton)
    {
        bags.Add(bag);
        bagButton.MyBag = bag;
    }

    public void RemoveBag(Bag bag)
    {
        bags.Remove(bag);
        Destroy(bag.myBagScript.gameObject);
    }

    public void SwapBags(Bag oldBag, Bag newBag)
    {
        var newSlotCount = MyTotalSlotCount - oldBag.Slots + newBag.Slots;

        if (newSlotCount - MyFullSlotCount >= 0)
        {
            //Do the swap
            var bagItems = oldBag.myBagScript.GetItems();

            newBag.MyBagButton = oldBag.MyBagButton;
            RemoveBag(oldBag);
            ((IUseable) newBag).Use();

            foreach (var item in bagItems)
                if (item != newBag)
                    AddItem(item);

            AddItem(oldBag);

            HandScript.instance.Drop();
            instance.fromSlot = null;
        }
    }

    public void AddItem(Item item)
    {
        if (item.MyStackSize > 0)
            if (PlaceInStack(item))
                return;

        PlaceInEmpty(item);
    }

    private void PlaceInEmpty(Item item)
    {
        foreach (var bag in bags)
            if (bag.myBagScript.AddItem(item))
                return;
    }

    private bool PlaceInStack(Item item)
    {
        foreach (var bag in bags)
        foreach (var slots in bag.myBagScript.Slots)
            if (slots.StackItem(item))
                return true;

        return false;
    }

    public void OpenClose()
    {
        bool closedBag = bags.Find(x => !x.myBagScript.IsOpened);
        //if closed bag == true, open all bags
        //if closed bag == false, close all bags

        foreach (var bag in bags)
            if (bag.myBagScript.IsOpened != closedBag)
                bag.myBagScript.OpenClose();
    }

    #region  Singleton

    public static InventoryScript instance;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    #endregion
}