using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Bag",menuName ="Inventory/Bag", order = 1)]
public class Bag : Item, IUseable
{

    private int slots;

    [SerializeField]
    private GameObject bagPrefab;

    public BagScript myBagScript { get; set; }

    public BagButton MyBagButton {get; set; }


    public int Slots { 
        get
        {
            return slots;
        }
    }


    public void Initialize(int slots)
    {
        this.slots = slots;
    }

    void IUseable.Use()
    {
        if(InventoryScript.instance.CanAddBag)
        {
            Remove();
            myBagScript = Instantiate(bagPrefab, InventoryScript.instance.transform).GetComponent<BagScript>();
            myBagScript.AddSlots(slots);
            if(MyBagButton == null)
            {
                InventoryScript.instance.AddBag(this);
            }
            else if(MyBagButton != null)
            {
                InventoryScript.instance.AddBag(this, MyBagButton);
            }
        }   

    }
}
