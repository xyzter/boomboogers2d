using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public Image icon;
    public Button remove;
    public Image clear_icon;
    Item item;

    public void AddItem(Item newItem)
    {
        item = newItem;

        icon.preserveAspect = true;
        icon.useSpriteMesh = true;
        icon.sprite = item.icon;
        icon.enabled = true;
        icon.color = new Color(255, 255, 255, 255);

        remove.gameObject.SetActive(true);
        remove.interactable = true;

    }

    public void ClearSlot()
    {
        item = null;

        icon.sprite = clear_icon.sprite;
        icon.enabled = false;
        icon.color = new Color(255, 255, 255, 0);

        remove.gameObject.SetActive(false);
        remove.interactable = false;
    }

    public void DropItem()
    {
        Debug.Log("drop");
        Inventory.instance.Remove(item);
    }

    public void UseItem()
    {
        if(item != null)
        {
            item.Use();
        }
    }
}
