using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    public static PlayerInteraction instance;
    [SerializeField] private float _distance;

    public int coins;

    [SerializeField] private Transform inventory_panel;
    [SerializeField] private Transform shop_panel;
    [SerializeField] private GameObject player_inv;
    [SerializeField] private GameObject soon_panel;
    private bool soon_open;

    [SerializeField] public GameObject[] weapons;
    [SerializeField] private GameObject big_map;
    [SerializeField] private GameObject normal_map;
    [SerializeField] private Camera map_cam;
    private bool toggle_map;

    public int kills, collected, damage_done;
    [SerializeField] private Text colete_moedas;
    [SerializeField] private Text mate_monstros;
    [SerializeField] private Text cause_dano;

    [SerializeField] private LayerMask coin_layer;
    
    

    public bool first = true, second = false;
    bool inv = false; 
    private void Awake()
    {
        instance = this;
        player_inv.SetActive(false);
        inventory_panel.gameObject.SetActive(false);
        shop_panel.gameObject.SetActive(false);
        ChangeDamage();
        FindAll();
    }

    private void Start()
    {
        mate_monstros.text = "Kill " + mate_monstros.GetComponent<MissionObjective>().kills + " monsters - " + kills;
        colete_moedas.text = "Collect " + colete_moedas.GetComponent<MissionObjective>().collect_number + " coins - " +
                             collected;
        cause_dano.text = "Cause " + cause_dano.GetComponent<MissionObjective>().inflict_damage + " damage - " +
                          damage_done;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            inv = !inv;
            player_inv.SetActive(inv);
        }
        if(Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
 
            if(hit.collider != null)
            {
                if(Vector2.Distance(this.transform.position, hit.collider.gameObject.transform.position) <= _distance)
                {
                    if(hit.collider.tag == "Chest")
                    {
                        soon_panel.gameObject.SetActive(true);
                        soon_open = true;
                        return;
                    }
                    if(hit.collider.tag == "Shop")
                    {
                        soon_panel.gameObject.SetActive(true);
                        soon_open = true;
                        return;
                    }
                }

            }
        }

        if(Input.anyKeyDown && soon_open)
        {
            soon_open = false;
            soon_panel.SetActive(false);
        }

        if(Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
 
            if(hit.collider != null)
            {
                if(Vector2.Distance(this.transform.position, hit.collider.gameObject.transform.position) <= _distance)
                {

                    if(hit.collider.tag == "Dungeon")
                    {
                        Debug.Log("Entrei na dungeon");
                    }
                }

            }
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            //toggle_map = !toggle_map;
        }

        if (!toggle_map)
        {
            map_cam.fieldOfView = 100f;
            big_map.SetActive(false);
            normal_map.SetActive(true);
        }
        else
        {
            map_cam.fieldOfView = 150f;
            big_map.SetActive(true);
            normal_map.SetActive(false);
        }
        
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            Attack.instance.is_melee = true;
            Attack.instance.weapon.gameObject.SetActive(false);
            first = true;
            second = false;
            ChangeDamage();
        }
        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            Attack.instance.is_melee = false;
            Attack.instance.weapon.gameObject.SetActive(true);
            first = false;
            second = true;
            ChangeDamage();
        }

    }

    private void FixedUpdate()
    {
        if (Physics2D.OverlapCircle(transform.position, 5, coin_layer))
        {
            
        }
    }

    public void ClosePersonalinventory()
    {
        inv = false;
        player_inv.SetActive(inv);
    }

    public void CloseInventory()
    {
        inventory_panel.gameObject.SetActive(false);
    }

    public void CloseShop()
    {
        shop_panel.gameObject.SetActive(false);
    }
    void FindAll()
    {
        //inventory_panel = this.transform.Find(inventory_name);
    }

    public void ChangeDamage()
    {
        if(first)
        {
            WeaponStats _temp = this.weapons[0].GetComponent<WeaponStats>();
            Attack _player = this.GetComponent<Attack>();

            _player.weapon_power = _temp.dmg;
            _player.weapon_mana_power = _temp.mana_dmg;
            _player.weapon_speed = _temp.speed;
            _player.weapon_mana_cost = _temp.mana_cost;
            _player.cooldown_time = _temp.cooldown_time;
            _player.is_melee = _temp.is_melee;
            return;
        }
        if(second)
        {
            WeaponStats _temp = this.weapons[1].GetComponent<WeaponStats>();
            Attack _player = this.GetComponent<Attack>();

            _player.weapon_power = _temp.dmg;
            _player.weapon_mana_power = _temp.mana_dmg;
            _player.weapon_speed = _temp.speed;
            _player.weapon_mana_cost = _temp.mana_cost;
            _player.cooldown_time = _temp.cooldown_time;
            _player.is_melee = _temp.is_melee;
            return;
        }
    }
    
    public void UpdateKill()
    {
        if (mate_monstros.GetComponent<MissionObjective>().kill &&
            kills < mate_monstros.GetComponent<MissionObjective>().kills)
        {
            kills++;
            mate_monstros.text = "Kill " + mate_monstros.GetComponent<MissionObjective>().kills + " monsters - " + kills.ToString();
        }
        if (mate_monstros.GetComponent<MissionObjective>().kill &&
            kills == mate_monstros.GetComponent<MissionObjective>().kills && !mate_monstros.GetComponent<MissionObjective>().finished)
        {
            mate_monstros.GetComponent<MissionObjective>().finished = true;
            //XPBar.instance.GetXp(300);
        }
    }

    public void UpdateCollect()
    {
        if (colete_moedas.GetComponent<MissionObjective>().collect &&
            collected < colete_moedas.GetComponent<MissionObjective>().collect_number)
        {
            collected++;
            colete_moedas.text = "Colect " + colete_moedas.GetComponent<MissionObjective>().collect_number + " coins - " + collected.ToString();
        }
        if (colete_moedas.GetComponent<MissionObjective>().collect &&
            collected == colete_moedas.GetComponent<MissionObjective>().collect_number && !colete_moedas.GetComponent<MissionObjective>().finished)
        {
            colete_moedas.GetComponent<MissionObjective>().finished = true;
            //XPBar.instance.GetXp(300);
        }
    }

    public void UpdateDamage(int dmg)
    {
        if (cause_dano.GetComponent<MissionObjective>().inflict &&
            damage_done < cause_dano.GetComponent<MissionObjective>().inflict_damage)
        {
            damage_done += dmg;
            cause_dano.text = "Cause " + cause_dano.GetComponent<MissionObjective>().inflict_damage + " damage - " + damage_done.ToString();
        }
        if (cause_dano.GetComponent<MissionObjective>().inflict &&
            damage_done >= cause_dano.GetComponent<MissionObjective>().inflict_damage && !cause_dano.GetComponent<MissionObjective>().finished)
        {
            cause_dano.GetComponent<MissionObjective>().finished = true;
            //XPBar.instance.GetXp(300);
        }
    }

    
}
