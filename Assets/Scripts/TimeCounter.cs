using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCounter : MonoBehaviour
{
    public float timeRemaining = 0;


    void Update()
    {
        timeRemaining += Time.deltaTime;
        
        DisplayTime(timeRemaining);
    }
    
    void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);  
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        GetComponent<Text>().text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
