using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotScript : MonoBehaviour, IPointerClickHandler, IClickable
{
    private ObservableStack<Item> items = new ObservableStack<Item>();

    [SerializeField]
    private Image icon;

    [SerializeField]
    private Text stackSize;

    public BagScript MyBag {get; set; }

    public bool IsEmpty
    {
        get
        {
            return MyItems.Count == 0;
        }
    }

    public bool IsFull
    {
        get
        {
            if (IsEmpty || MyCount < MyItem.MyStackSize)
            {
                return false;
            }
            return true;
        }
    }

    public Item MyItem
    {
        get
        {
            if(!IsEmpty)
            {
                return MyItems.Peek();
            }

            return null;
        }
    }

    public Image myIcon { 
        get
        {
            return icon;
        }
        set
        {
            icon = value;
        }
    }

    public int MyCount 
    {
        get
        {
            return MyItems.Count;
        }
    }

    public Text MyStackText
    {
        get
        {
            return stackSize;
        }
    }

    public ObservableStack<Item> MyItems { 
            get
            {
                return items;
            }
         }

    private void Awake() {
        MyItems.OnPop += new UpdateStackEvent(UpdateSlot);
        MyItems.OnPush += new UpdateStackEvent(UpdateSlot);
        MyItems.OnClear += new UpdateStackEvent(UpdateSlot);
    }


    public bool AddItem(Item item)
    {
        MyItems.Push(item);
        icon.sprite = item.MyIcon;
        icon.color = Color.white;
        item.MySlot = this;
        return true;
    }

    public bool AddItems(ObservableStack<Item> newItems)
    {
        if (IsEmpty || newItems.Peek().GetType() == MyItem.GetType())
        {
            int count = newItems.Count;

            for (int i = 0; i < count; i++)
            {
                if (IsFull)
                {
                    return false;
                }

                AddItem(newItems.Pop());
            }
            return true;
        }

        return false;
    }   

    public void RemoveItem(Item item)
    {
        if(!IsEmpty)
        {
            MyItems.Pop();
        }
    }

    public void Clear()
    {
        if (MyItems.Count > 0)
        {
            MyItems.Clear();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (InventoryScript.instance.FromSlot == null && !IsEmpty)//without having something to move
            {
                if (HandScript.instance.MyMoveable != null && HandScript.instance.MyMoveable is Bag)
                {
                    if (MyItem is Bag)
                    {
                        InventoryScript.instance.SwapBags(HandScript.instance.MyMoveable as Bag, MyItem as Bag); 
                    }
                }
                else
                {
                    HandScript.instance.TakeMoveable(MyItem as IMoveable);
                    InventoryScript.instance.FromSlot = this;
                }
            }
            else if (InventoryScript.instance.FromSlot == null && IsEmpty && (HandScript.instance.MyMoveable is Bag))
            {
                Bag bag = (Bag)HandScript.instance.MyMoveable;

                if (bag.myBagScript != MyBag && InventoryScript.instance.MyEmptySlotCount - bag.Slots > 0)
                {
                    AddItem(bag);
                    bag.MyBagButton.RemoveBag();
                    HandScript.instance.Drop();
                }
                
            }
            else if (InventoryScript.instance.FromSlot != null)//if we have something to move
            {
                if (PutItemBack() || MergeItems(InventoryScript.instance.FromSlot) || SwapItems(InventoryScript.instance.FromSlot) || AddItems(InventoryScript.instance.FromSlot.MyItems))
                {
                    HandScript.instance.Drop();
                    InventoryScript.instance.FromSlot = null;
                }
            }

        }
        if(eventData.button == PointerEventData.InputButton.Right)
        {
            UseItem();
        }
    }

    public void UseItem()
    {
        if(MyItem is IUseable)
        {
            (MyItem as IUseable).Use();
        }
    }

    public bool StackItem(Item item)
    {

        if (!IsEmpty && item.name == MyItem.name && MyItems.Count < MyItem.MyStackSize)
        {
            MyItems.Push(item);
            item.MySlot = this;
            return true;
        }
        return false;
    }

    private bool PutItemBack()
    {
        if (InventoryScript.instance.FromSlot == this)
        {
            InventoryScript.instance.FromSlot.myIcon.color = Color.white;
            return true;
        }

        return false;
    }

    private bool SwapItems(SlotScript from)
    {
        if (IsEmpty)
        {
            return false;
        }
        if (from.MyItem.GetType() != MyItem.GetType() || from.MyCount + MyCount > MyItem.MyStackSize)
        {
            //Copy all items we need to swap from A
            ObservableStack<Item> tempFrom = new ObservableStack<Item>(from.MyItems);
            
            //Clear Slot A
            from.MyItems.Clear();
            //All items from Slot B and Copy them to A
            from.AddItems(MyItems);
            //Clear B
            MyItems.Clear();
            //Move the items from ACopy to B
            AddItems(tempFrom);

            return true;
        }

        return false;
    }

    private bool MergeItems(SlotScript from)
    {
        if (IsEmpty)
        {
            return false;
        }
        if (from.MyItem.GetType() == MyItem.GetType() && !IsFull)
        {
            //How many free slots we have
            int free = MyItem.MyStackSize - MyCount;

            for (int i = 0; i < free; i++)
            {
                AddItem(from.MyItems.Pop());
            }

            return true;
        }
        return false;
    }

    private void UpdateSlot()
    {
        UIManager.instance.UpdateStackSize(this);

    }
}
