using UnityEngine;
using UnityEngine.UI;

public class XPBar : MonoBehaviour
{
    public static XPBar instance;

    private float extraXp;

    private int maxValueInt;

    [HideInInspector]public int level;

    public Slider slider;
    public Text xpText;

    private void Awake()
    {
        instance = this;
        SetMaxValue(100);
        maxValueInt = (int) slider.maxValue;
    }

    private void Start()
    {
        slider.value = 0;
        UpdateText();
    }

    public void Update()
    {
        xpText.color = slider.value >= slider.maxValue / 2 ? Color.black : Color.white;

        //if (Input.GetKeyDown(KeyCode.N)) GetXp(90);

        if (slider.value >= slider.maxValue)
        {
            SetMaxValue(slider.maxValue + 100 * 2.2f);
            Healthbar.instance.SetMaxValue(Healthbar.instance.slider.maxValue + 100 * 1.7f);
            Healthbar.instance.Heal(Healthbar.instance.slider.maxValue);
            ManaBar.instance.SetMaxValue(ManaBar.instance.slider.maxValue + 100 * 1.2f);
            ManaBar.instance.GetMana(ManaBar.instance.slider.maxValue);
            level++;
            //InfoPanel.instance.UpdateInfo();

            ResetValue(0);
        }
    }

    private void SetMaxValue(float value)
    {
        slider.maxValue = value;
        var temp = (int) slider.maxValue;
        maxValueInt = temp;
        UpdateText();

        //Upgrade Character Stats
    }

    public void GetXp(float value)
    {
        if (slider.value + value > slider.maxValue) extraXp = slider.value + value - slider.maxValue;

        slider.value += value;
        UpdateText();
    }

    public void ResetValue(float value)
    {
        slider.value = value + extraXp;
        UpdateText();
    }

    public void UpdateText()
    {
        xpText.text = slider.value + "/" + maxValueInt;
    }
}