using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [HideInInspector] public float ArrowVelocity;
    [SerializeField] Rigidbody2D rb;

    [HideInInspector] public float dmg;

    private void Start() {
        Destroy(gameObject, 3f);
    }

    private void FixedUpdate() {
        rb.velocity = transform.up * ArrowVelocity;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        Destroy(gameObject);
        if(other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<EnemyAI>().TakeDamage(dmg);
        }
    }
}
