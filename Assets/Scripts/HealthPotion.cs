using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HealthPotion", menuName = "Inventory/HealthPot", order =1)]
public class HealthPotion : Item, IUseable
{
    //Sprite IUseable.MyIcon => throw new System.NotImplementedException();

    [SerializeField]
    private int health;

    void IUseable.Use()
    {

        if (CharacterStats.instance.currentHealth < CharacterStats.instance.maxHealth)
        {
            Remove();
            CharacterStats.instance.currentHealth += health;
        }
    }
}
