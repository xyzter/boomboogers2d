using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragWindow : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerDownHandler
{
    [SerializeField] private RectTransform drag_rect;
    [SerializeField] private Canvas canvas;
    [SerializeField] private Image background_image;
    private Color bg_color;

    private void Awake() {
       bg_color = background_image.color;
       
       if(drag_rect == null)
       {
           drag_rect = transform.parent.GetComponent<RectTransform>();
       }
       if(canvas == null)
       {
           Transform testCanvas = transform.parent;
           while(testCanvas != null)
           {
               canvas = testCanvas.GetComponent<Canvas>();
               if(canvas != null){
                   break;
               }
               testCanvas = testCanvas.parent;
           }
       }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        bg_color.a = .4f;
        background_image.color = bg_color;
    }

    public void OnDrag(PointerEventData eventData)
    {
        drag_rect.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        bg_color.a = 1f;
        background_image.color = bg_color;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        drag_rect.SetAsLastSibling();
    }
}
