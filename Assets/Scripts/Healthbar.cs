using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    
    public static Healthbar instance;
    private int currentValue;

    private float damage;
    public Image health;
    public Text hpText;

    public float armor;

    private bool loseHealth;
    //public Color healthyColor;
    //public Color warningColor;

    private int maxValueInt;
    private float oldValue;
    public float max_value;

    public Slider slider;

    private void Awake()
    {
        instance = this;
        SetMaxValue(100);
        maxValueInt = (int) slider.maxValue;
        Heal(slider.maxValue);
        currentValue = (int) slider.value;
        hpText.text = currentValue + "/" + maxValueInt;
    }

    private void Update()
    {
        if (slider.value > slider.maxValue) slider.value = slider.maxValue;


        //if (Input.GetKeyDown(KeyCode.M)) SetDamageValue(50);
        //if (Input.GetKeyDown(KeyCode.B)) Heal(Random.Range(15, 30));


        if (slider.value <= 0) loseHealth = false;
        if (currentValue <= 0) SceneManager.LoadScene(1);

        if (loseHealth)
        {
            slider.value -= Time.deltaTime * 30;
            currentValue = (int) slider.value;
            if (oldValue - slider.value >= damage)
            {
                damage = 0;
                loseHealth = false;
                if (slider.value <= slider.maxValue * 0.4f)
                {
                    //health.color = warningColor;
                }
            }
        }
    }

    public void SetMaxValue(float value)
    {
        slider.maxValue = value;
        maxValueInt = (int) slider.maxValue;
        currentValue = (int) slider.value;
        hpText.text = currentValue + "/" + maxValueInt;
    }

    public void SetDamageValue(float value)
    {
        damage = (value - armor);
        armor -= value;

        if (armor < 0) armor = 0;

        if (damage <= 0) return;
        
        if (currentValue - damage < 0) damage = currentValue;

        oldValue = slider.value;
        loseHealth = true;
        currentValue = (int) (slider.value - value);
        hpText.text = currentValue + "/" + maxValueInt;

        if (slider.value <= slider.maxValue * 0.4f)
        {
            //health.color = warningColor;
        }
    }

    public void Heal(float value)
    {
        slider.value += value;
        currentValue = (int) slider.value;
        hpText.text = currentValue + "/" + maxValueInt;
        loseHealth = false;
        if (slider.value <= slider.maxValue * 0.4f)
        {
            //health.color = warningColor;
        }
    }
}