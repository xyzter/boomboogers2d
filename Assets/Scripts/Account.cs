using System.Collections;
using System.Collections.Generic;
// Here we import the Netherum.JsonRpc methods and classes.
using Nethereum.JsonRpc.UnityClient;
using UnityEngine;

public class Account : MonoBehaviour
{

	public MenuManager menu_manager;
	public string wallet;
	void Start () {
		StartCoroutine(getAccountBalance(wallet, (balance) => {
			Debug.Log(balance);
			menu_manager.InitiateLoading(1);
		}));
		CreateAccount ("strong_password", (address, encryptedJson) => {
			Debug.Log (address);
			Debug.Log (encryptedJson);
			
			StartCoroutine (getAccountBalance (address, (balance) => {
				Debug.Log (balance);
			}));
		});

		
	}

	public static IEnumerator getAccountBalance (string address, System.Action<decimal> callback) {
		var getBalanceRequest = new EthGetBalanceUnityRequest ("https://mainnet.infura.io/v3/7e6d39c91e794730b64b205f08d68d39");
		yield return getBalanceRequest.SendRequest(address, Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest ());
		
		if (getBalanceRequest.Exception == null) {
			var balance = getBalanceRequest.Result.Value;
			callback (Nethereum.Util.UnitConversion.Convert.FromWei(balance, 18));
		} else {
			throw new System.InvalidOperationException ("Get balance request failed");
			
		}

	}
	public void CreateAccount (string password, System.Action<string, string> callback) {
		var ecKey = Nethereum.Signer.EthECKey.GenerateKey();

		var address = ecKey.GetPublicAddress();
		var privateKey = ecKey.GetPrivateKeyAsBytes();

		var keystoreservice =  new Nethereum.KeyStore.KeyStoreService();
		
		var encryptedJson = keystoreservice.EncryptAndGenerateDefaultKeyStoreAsJson (password, privateKey, address);
		callback (address, encryptedJson);
	}
}