using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SurvivalPoints : MonoBehaviour
{
    #region singleton

    public static SurvivalPoints instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion
    
    
    private int points;
    
    public void UpdatePoints()
    {
        points++;
        GetComponent<Text>().text = "PONTOS: " + points.ToString();
    }

}
