using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MissionObjective : MonoBehaviour
{
    public bool kill, collect, inflict;

    public int kills, collect_number, inflict_damage;

    public bool finished;

    private void Start()
    {
        kills = (int) Random.Range(3, 6);
        collect_number = (int) Random.Range(3, 6);
        inflict_damage = (int) Random.Range(0, 300);
    }
    
    
}
