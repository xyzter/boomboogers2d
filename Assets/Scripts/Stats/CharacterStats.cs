using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{

    #region  Singleton
    public static CharacterStats instance;
    #endregion

    public int maxHealth = 100;
    public int currentHealth;
    public Stat damage;
    public Stat armor;

    private void Awake() {
        currentHealth = maxHealth;
        if(instance == null) instance = this;
    }
    private void Update() {
        if(Input.GetKeyDown(KeyCode.T))
        {
            TakeDamage(10);
        }
    }

    public void TakeDamage(int dmg)
    {
        dmg -= armor.GetValue();

        dmg = Mathf.Clamp(dmg, 0, int.MaxValue);

        currentHealth -= dmg;
        Debug.Log(transform.name + " takes " + dmg + " damage.");

        if(currentHealth <= 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        Debug.Log(transform.name + " died.");
    }
    
}
