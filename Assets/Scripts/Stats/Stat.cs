using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat 
{

    [SerializeField]
    public int baseValue;

    private List<int> modifiers = new List<int>();

    public int GetValue()
    {
        int finalValue = baseValue;
        modifiers.ForEach(x => finalValue += x);
        return finalValue;
    }

    public void AddMod(int modifier)
    {
        if(modifier != 0) modifiers.Add(modifier);
    }

    public void RemoveMod(int modifier)
    {
        if(modifier != 0) modifiers.Remove(modifier);
    }
}
