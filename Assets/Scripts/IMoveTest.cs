using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface IMoveTest
{
    bool IsIdle();
    void MoveTo(Vector3 position, float stopDistance, Action onArrivedAtPosition);
}
